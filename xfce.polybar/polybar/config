;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;==========================================================


[colors]
;background = ${xrdb:color0:#222}
background = #4f0808
crimsonx = #4f0808 
;this is an important color
bkg = #fff
;this one too
background-alt = #444
;foreground = #fff
foreground = #fff
foreground-alt = #fff
primary = #ffb52a
secondary = #bd2c40
alert = #bd2c40

[bar/example]
;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 20
underline-size=0
;offset-x = 0%
;offset-y = 0%
radius = 0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 0

module-margin-left = 0
module-margin-right = 0

;======
;FONTS
;======


font-0 = "HelveticaNeue :weight=bold:pixelsize=12;2"
font-1 = unifont:fontformat=truetype:size=12:antialias=true;0
font-2 = "Wuncon Siji:pixelsize=12;1"
font-3 = "Source Code Variable:size=15;4"

;==============
;POLYBAR LAYOUT
;==============

modules-left = coolSpace mpd leftA 
modules-center = leftA2 date rightA2
modules-right =  rightA filesystem rightA2 alsa rightA xkeyboard rightA2 memory rightA  cpu  rightA2 eth rightA temperature coolSpace

; modules-right = filesystem xbacklight alsa pulseaudio xkeyboard memory ;cpu wlan eth battery temperature date powermenu

;===============

tray-position = right
tray-padding = 2
;tray-transparent = true
;tray-background = #0063ff

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

;=============
;MODULES
;=============

[module/xwindow]
type = internal/xwindow
label = %title:0:15:...%

;==================
;CAPS LOCK MODULE
;==================

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.crimsonx}
format-prefix-background = ${colors.bkg}
;format-prefix-underline = ${colors.bkg}

label-layout = "%layout%"
label-layout-background = ${colors.bkg}
label-layout-foreground = ${colors.crimsonx}
;label-layout-underline = ${colors.bkg}
label-indicator-padding = 0
label-indicator-margin = 0
label-indicator-foreground = ${colors.crimsonx}
label-indicator-background = ${colors.bkg}
label-indicator-underline = ${colors.bkg}

;==========
;FS MODULE
;==========
[module/filesystem]
type = internal/fs
interval = 25
mount-0 = /
label-mounted = " %mountpoint%: %percentage_used%% "
label-mounted-foreground = ${colors.crimsonx}
label-mounted-background = ${colors.bkg}
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}
;=============

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

; Separator in between workspaces
; label-separator = |

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

; Separator in between workspaces
; label-separator = |

;===========
;MPD MODULE
;===========

[module/mpd]
type = internal/mpd
format-online = "<label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>"
format-online-foreground = ${colors.crimsonx}
format-online-background = ${colors.bkg}
host = 127.0.0.1
port = 6600
icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

;================
;BACKLIGHT MODULE
;================

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = ${colors.bkg}
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

;
;BACKLIGHT F
;
[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

;================
;CPU MODULE
;================

[module/cpu]
type = internal/cpu
interval = 2.0
format-prefix = " "
format-prefix-foreground = ${colors.crimsonx}
format-prefix-background = ${colors.bkg}
;format-underline = ${colors.bkg}
label-background = ${colors.bkg}
label-foreground = ${colors.crimsonx}
label = "%percentage:2%% "

;================
;MEMORY MODULE
;================

[module/memory]
type = internal/memory
interval = 3.0
format-prefix = " "
format-prefix-foreground = ${colors.bkg}
format-underline = ${colors.bkg}
label = "%percentage_used%% "
label-foreground = ${colors.bkg}

;================
;WLAN MODULE
;================

[module/wlan]
type = internal/network
interface = net1
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = #9f78e1
label-connected = %essid%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.foreground-alt}

;========================
;WIRED CONNECTION MODULE
;========================

[module/eth]
type = internal/network
interface = eno1
interval = 60

format-connected-underline = ${colors.bkg}
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.bkg}
label-connected = "%local_ip% "
label-connected-foreground = ${colors.bkg}

;format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${colors.bkg}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.bkg}

;============
;DATE MODULE
;============

[module/date]
type = internal/date
interval = 5

date =" %Y / %b / %d  "
date-alt = 

time = "%I:%M %p"
time-alt = %H:%M:%S

format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}
;format-underline = ${colors.bkg}

label = %date% %time%
label-foreground=${colors.crimsonx}
label-background=${colors.bkg}

;========================
;DEPRECATED VOLUME MODULE
;=======================

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = 🔇 muted
label-muted-foreground = #666
bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = o
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

;=============
;AUDIO CONTROL
;=============
[module/alsa]
type = internal/alsa

format-volume = "<label-volume> <bar-volume> "
label-volume = "%percentage%%"
label-volume-underline=${colors.bkg}
label-volume-foreground = ${colors.bkg}

format-muted-prefix = "  "
format-muted-foreground = ${colors.bkg}
label-muted = muted
bar-volume-width = 8
bar-volume-foreground-0 = ${colors.bkg}
bar-volume-foreground-1 = ${colors.bkg}
bar-volume-foreground-2 = ${colors.bkg}
bar-volume-foreground-3 = ${colors.bkg}
bar-volume-foreground-4 = ${colors.bkg}
bar-volume-foreground-5 = ${colors.bkg}
bar-volume-foreground-6 = ${colors.bkg}
bar-volume-gradient = false
bar-volume-indicator = ●
bar-volume-indicator-font = 6
bar-volume-indicator-foreground = ${colors.bkg}
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT0
adapter = ADP1
full-at = 98

format-charging = <animation-charging> <label-charging>
format-charging-underline = #ffb52a

format-discharging = <animation-discharging> <label-discharging>
format-discharging-underline = ${self.format-charging-underline}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = ${colors.foreground-alt}
animation-discharging-framerate = 750

;==================
;TEMPERATURE MODULE
;==================

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60
interval = 3
format = <ramp><label>
format-underline = ${colors.bkg}
format-warn = "<ramp><label-warn>"
format-warn-underline = ${self.format-underline}

label-background=${colors.bkg}
label-foreground=${colors.crimsonx}
label = "%temperature-c%"
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-background = ${colors.bkg}
ramp-foreground = ${colors.crimsonx}

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.secondary}
label-close =  cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini

;============
; TEXT MODULES
;============

[module/space]
type = custom/text
content = " "
content-foreground = transparent
content-background = transparent
content-padding = 1

[module/coolSpace]
type = custom/text
content = "  "
content-foreground = ${colors.bkg}
content-background = ${colors.bkg}
content-padding = 1

[module/rightA]
type = custom/text
content = "%{T4}%{T-}"
content-foreground = ${colors.bkg}
content-background = transparent

[module/leftA]
type = custom/text
content = "%{T4}%{T-}"
content-foreground = ${colors.bkg}
content-background = transparent

[module/rightA2]
type = custom/text
content = "%{T4}%{T-}"
content-foreground = ${colors.crimsonx}
content-background = ${colors.bkg}

[module/leftA2]
type = custom/text
content = "%{T4}%{T-}"
content-foreground = ${colors.crimsonx}
content-background = ${colors.bkg}
